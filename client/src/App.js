import React, {Component} from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import "./styles/app.css"

import Navigation from './components/Navigation';
import Sidebar from './components/routes/sidebar/Sidebar';
import Footer from './components/Footer';

import Dashboard from './components/routes/dashboard/Dashboard';
import Login from './components/routes/Login';
import Register from './components/routes/Register';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="app-container">
          <Navigation />
          <div className="container">
            <div className="row">
              <div className="col s12 l8 high">
                <Route exact path="/" component={Dashboard} />
                <Route path="/login" component={Login} />
                <Route path="/register" component={Register} />
              </div>
              <div className="col l4 hide-on-med-and-down high">
                <Sidebar />
              </div>
            </div>
          </div>
          <Footer />
        </div>
      </BrowserRouter>
    )
  }
}

export default App;