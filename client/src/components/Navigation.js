import React, {Component} from 'react';
import { Link } from 'react-router-dom';

class Navigation extends Component {
  constructor(props) {
    super(props)
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick() {
    console.log('clicked logout');
  }
  render() {
    return (
    <div className="navbar-fixed">
      <nav className="light-blue lighten-1">
        <div className="nav-wrapper container">
          <div className="row">
            <Link to="/" className="brand-logo col m4"> <i className="material-icons">adjust</i> Bloggie</Link>
            <div className="right col m4">
              <p className="hide-on-med-and-down left">Logged in as <span id="username"> Anonymous </span></p>
              <Link to="/login" onClick={this.handleClick} className="right"> <span className="material-icons">exit_to_app</span> </Link>
            </div>
          </div>
        </div>
      </nav>
    </div>
    )
  }
}

export default Navigation;