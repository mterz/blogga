import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../styles/components/register.css'

import RegisterService from '../../services/RegisterService';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      email: ''
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(e) {
    this.setState({
      [e.target.id]: e.target.value
    })
  }
  handleSubmit(e) {
    e.preventDefault();

    RegisterService.register(this.state).then(res => {

      this.setState({
        username: '',
        password: '',
        email: ''
      })

      console.log(res);
    })
  }
  render() {
    return (
      <div className="form-container valign-wrapper">
        <div className="container">
          <form onSubmit={this.handleSubmit}>
            <div className="row">
              <div className="input-field col offset-l3 l6 offset-s2 s8">
                <i className="material-icons prefix">account_circle</i>
                <input id="username" type="text" className="validate" onChange={this.handleChange} value={this.state.username} required/>
                <label htmlFor="username">Username</label>
              </div>
              <div className="input-field col offset-l3 l6 offset-s2 s8">
                <i className="material-icons prefix">lock</i>
                <input id="password" type="password" className="validate" onChange={this.handleChange} value={this.state.password} required/>
                <label htmlFor="password">Password</label>
              </div>
              <div className="input-field col offset-l3 l6 offset-s2 s8">
                <i className="material-icons prefix">email</i>
                <input id="email" type="email" className="validate" onChange={this.handleChange} value={this.state.email} required/>
                <label htmlFor="email">Email</label>
              </div>
              <button type="submit" className="waves-effect light-blue lighten-1 waves-light btn col offset-l4 l4 offset-s3 s6" id="register-button">Register</button>
            </div>
            <p className="center">Already have an account? <Link to="/login">Login</Link></p>
          </form>
        </div>
      </div>
    );
  }
}

export default Register;