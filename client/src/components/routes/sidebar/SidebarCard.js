import React from 'react';
import {Link} from 'react-router-dom'

const SidebarCard = (props) => {
  return (
    <div className="col s12 m12">
      <div className="card horizontal">
        <div className="card-image">
          <img src="https://placekitten.com/100/190" />
        </div>
        <div className="card-stacked">
          <div className="card-content">
            <h5 className="header">{props.data.title}</h5>
            <p>{props.data.description}</p>
          </div>
          <div className="card-action">
            <Link to={props.data.link}>Open</Link>
          </div>
        </div>
      </div>
    </div>
  )
}

export default SidebarCard;