import React, {Component} from 'react';
import SidebarCard from './SidebarCard';

class Sidebar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: {
        title: 'Sidebar card',
        link: '#',
        description: 'blah blha dsscds blahgh'
      }
    }
  }
  render() {
    return (
      <div>
        <h3 className="center">Most Popular</h3>
        <SidebarCard data={this.state.data}/>
        <SidebarCard data={this.state.data}/>
        <SidebarCard data={this.state.data}/>
      </div>
    )
  }
}

export default Sidebar;