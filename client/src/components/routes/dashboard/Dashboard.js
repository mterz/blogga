import React, {Component} from 'react';
import BlogCard from './BlogCard';

class Dashboard extends Component{
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="row">
        <BlogCard />
        <BlogCard />
        <BlogCard />
        <BlogCard />
        <BlogCard />
        <BlogCard />
        <BlogCard />
      </div>
    )
  }
}

export default Dashboard;