import React from 'react';
import { Link } from 'react-router-dom';

const BlogCard = (props) => {
  return (
    <div className="col s12 m6">
      <div className="card">
        <div className="card-image">
          <img src="https://placekitten.com/300/200" />
          <span className="card-title">Card Title</span>
          <Link to="/blogs/1" className="btn-floating halfway-fab waves-effect waves-light light-blue lighten-1"><i className="material-icons">add</i></Link>
        </div>
        <div className="card-content">
          <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
        </div>
      </div>
    </div>
  )
}

export default BlogCard;