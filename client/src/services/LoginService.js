import Api from './Api'

export default {
  login ({username, password}) {
    return Api().post('/login', {
      username,
      password
    })
  }
}
