import Api from './Api'

export default {
  register ({username, password, email}) {
    return Api().post('/register', {
      username,
      password,
      email
    })
  }
}
