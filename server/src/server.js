const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();

const PORT = process.env.PORT || 3000;

app.use(cors());
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send("HELLLO from tha servuur");
})

app.post('/login', (req, res) => {
  console.log(req.body);

  res
    .status(200)
    .send({
      data: req.body,
      "x-auth": "1234",
    })
})

app.post('/register', (req, res) => {
  console.log(req.body);

  res
    .status(200)
    .send({
      data: req.body,
      "x-auth": "1234",
    })
})

app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
})